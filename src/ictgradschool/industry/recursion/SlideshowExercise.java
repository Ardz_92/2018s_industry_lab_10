package ictgradschool.industry.recursion;

public class SlideshowExercise {
    public static void main(String[] args){
        SlideshowExercise rs = new SlideshowExercise();
        rs.start();
    }

    private void start() {
        System.out.println(reverse("cat"));
    }

    public String reverse(String input){
        if(input.length()==1){
            return input;
        }

        return(input.charAt(input.length()-1))+(reverse(input.substring(0, input.length()-1)));
    }
}
